package ca.claurendeau.jmeter;

public class MyProcessB implements MyProcess{

    @Override
    public String doSomething() {
        MyProcess.sleep(80);
        return "Process B has done something";
    }

}

package ca.claurendeau.jmeter;

public class MyProcessA implements MyProcess{

    @Override
    public String doSomething() {
        MyProcess.sleep(300);
        return "Process A has done something";
    }

}

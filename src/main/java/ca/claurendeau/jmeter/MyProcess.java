package ca.claurendeau.jmeter;

import java.util.Random;

public interface MyProcess {
    String doSomething();
    
    static MyProcess createRandomProcess() {
        switch (new Random().nextInt(5)) {
        case 0:
            return new MyProcessA();
        case 1:
            return new MyProcessB();
        case 2:
            return new MyProcessC();
        case 3:
            return new MyProcessD();
        case 4:
            return new MyProcessE();
        default:
            return new MyProcessA();
        }
    }
    
    static void sleep(int ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

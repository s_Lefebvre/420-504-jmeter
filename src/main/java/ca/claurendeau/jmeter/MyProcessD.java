package ca.claurendeau.jmeter;

public class MyProcessD implements MyProcess{

    @Override
    public String doSomething() {
        MyProcess.sleep(50);
        return "Process D has done something";
    }

}
